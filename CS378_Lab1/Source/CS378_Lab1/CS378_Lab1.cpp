// Copyright Epic Games, Inc. All Rights Reserved.

#include "CS378_Lab1.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, CS378_Lab1, "CS378_Lab1" );

DEFINE_LOG_CATEGORY(LogCS378_Lab1)
 
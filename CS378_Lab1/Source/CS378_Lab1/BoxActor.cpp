// Fill out your copyright notice in the Description page of Project Settings.


#include "BoxActor.h"
#include "Components/SphereComponent.h"

// Sets default values
ABoxActor::ABoxActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerComponent"));

	MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	CollisionComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void ABoxActor::BeginPlay()
{
	Super::BeginPlay();
	GetSphereComponent()->OnComponentBeginOverlap.AddDynamic(this, &ABoxActor::BeginOverlap);
}

// Called every frame
void ABoxActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABoxActor::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent*
	OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	if (OtherActor->IsA(APawn::StaticClass())) {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Orange, FString::Printf(TEXT("Begin Trigger Overlap")));
		}
		this->Destroy();
	}
}

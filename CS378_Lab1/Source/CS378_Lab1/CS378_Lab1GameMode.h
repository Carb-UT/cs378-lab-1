// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CS378_Lab1GameMode.generated.h"

UCLASS(MinimalAPI)
class ACS378_Lab1GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACS378_Lab1GameMode();
};




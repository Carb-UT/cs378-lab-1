// Copyright Epic Games, Inc. All Rights Reserved.

#include "CS378_Lab1GameMode.h"
#include "CS378_Lab1Pawn.h"

ACS378_Lab1GameMode::ACS378_Lab1GameMode()
{
	// set default pawn class to our character class
	//DefaultPawnClass = ACS378_Lab1Pawn::StaticClass();
	static ConstructorHelpers::FObjectFinder<UClass>pawnBPClass(TEXT("Class'/Game/Blueprints/CS378_Lab1PawnBP.CS378_Lab1PawnBP_C'"));

	if (pawnBPClass.Object) {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("PawnBP Found"));
		}
		UClass * pawnBP = (UClass*)pawnBPClass.Object;
		DefaultPawnClass = pawnBP;
	}
	else {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP NOT Found"));
		}
		DefaultPawnClass = ACS378_Lab1Pawn::StaticClass();
	}
}

